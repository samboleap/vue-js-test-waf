require('dotenv').config(); // Load environment variables
const express = require('express');
const wafris = require('wafris'); // Assuming 'wafris' exports a function that returns a middleware

const app = express();
const redisUrl = "redis://nXDHRPJX6bClmHHUXUmsgRpID5u6JNQmo3KeOWoqwa4@redis-dedicated-iad-1.fly.dev:9987"; // Use environment variable or default

(async () => {
  const wafrisMiddleware = await wafris(redisUrl); // Initialize the middleware asynchronously
  app.use(wafrisMiddleware);

  // Serve static files from the Vue.js build folder
  app.use(express.static('dist'));

  const PORT = process.env.PORT || 3000;
  app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
  });
})();
